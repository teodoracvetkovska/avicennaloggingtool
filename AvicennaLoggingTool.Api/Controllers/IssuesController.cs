﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AvicennaLoggingTool.Api.Models;
using AvicennaLoggingTool.Api.Models.DataModels;
using AvicennaLoggingTool.Api.Services;
using AvicennaLoggingTool.Api.ViewModels.IssueVModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AvicennaLoggingTool.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IssuesController : Controller
    {
        private readonly IIssueService _issueService;
        private UserManager<ApplicationUser> _userManager;


        public IssuesController(IIssueService project, UserManager<ApplicationUser> userManager)
        {
            _issueService = project;
            _userManager = userManager;
        }

        // GET api/values
        [HttpGet]
        [Route("")]
        public IActionResult GetAll()
        {
            var issues = _issueService.GetAllIssues();
            
            
        }
        

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get([FromRoute]long id)
        {
            var model = _issueService.GetIssueById(id);
            if (model == null)
                return NotFound();
            var user = _userManager.Users.FirstOrDefault(i => i.Id == model.UserId);
        
            var vmodel = new IssueDetailsModel()
            {
                ProjectName = model.Project.Name,
                Type = model.Type.Name,
                Status = model.Status.Name,
                AssignedUser = user.UserName ?? "",
                Completed = model.Completed
            };
            return Ok(vmodel);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}