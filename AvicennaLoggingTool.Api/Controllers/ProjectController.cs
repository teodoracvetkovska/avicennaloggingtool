﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using AvicennaLoggingTool.Api.Models;
using AvicennaLoggingTool.Api.Models.DataModels;
using AvicennaLoggingTool.Api.Services;
using AvicennaLoggingTool.Api.ViewModels.ProjectVModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AvicennaLoggingTool.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private IProjectService _project;
        private UserManager<ApplicationUser> _userManager;


        public ProjectController(ProjectService project,UserManager<ApplicationUser> userManager)
        {
            _project = project;
            _userManager = userManager;
        }

        // GET: api/Project
        [HttpGet]
        [Route("All")]
        public IActionResult GetAll()
        {
            var model = _project.GetAllProjects();
            var returnModel = new ProjectsListingModel()
            {
                Projects = model
            };

            return Ok(returnModel);
        }

        [HttpGet("{id}")]
        [Route("AllByUser")]
        public IActionResult GetAllByUser(long id)
        {
            var model = _project.GetAllProjectsByUser(id);
            var returnModel = new ProjectsListingModel()
            {
                Projects = model
            };

            return Ok(returnModel);
        }

        // GET: api/Project/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            return Ok(_project.GetProjectsById(id));
        }

        // POST: api/Project
        [HttpPost]
        public async IActionResult AddProject([FromBody] ProjectBindingModel bindingModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var user = _userManager.Users.SingleOrDefault(x => x.Id == bindingModel.CreatorId);
           var project = new Project()
           {
               CreatedOn=DateTime.Now,
               Description=bindingModel.Description,
               Name=bindingModel.Name,
               

               

           }     

            
        }

        // PUT: api/Project/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
