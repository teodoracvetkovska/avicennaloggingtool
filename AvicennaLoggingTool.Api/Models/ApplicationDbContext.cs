﻿using AvicennaLoggingTool.Api.Models.DataModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AvicennaLoggingTool.Api.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser,IdentityRole<long>,long>
    {


        public DbSet<BasicInfo> BasicInfos { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<LogTime> LogTimes { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectsUsers> ProjectsUsers { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Type> Types { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ProjectsUsers>()
                .HasKey(ps => new { ps.ApplicationUserId, ps.ProjectId });
            builder.Entity<ProjectsUsers>()
                .HasOne(ps => ps.Project)
                .WithMany(s => s.ProjectsUsers)
                .HasForeignKey(ps => ps.ProjectId);
            builder.Entity<ProjectsUsers>()
                .HasOne(ps => ps.ApplicationUser)
                .WithMany(s => s.ProjectsUsers)
                .HasForeignKey(ps => ps.ApplicationUserId);
        }
    }
}
