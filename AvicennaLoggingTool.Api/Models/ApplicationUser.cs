﻿using System.Collections.Generic;
using AvicennaLoggingTool.Api.Models.DataModels;
using Microsoft.AspNetCore.Identity;

namespace AvicennaLoggingTool.Api.Models
{
    public class ApplicationUser :IdentityUser<long>
    {
        [PersonalData]
        public ICollection<LogTime> TimeLogs { get; set; }
        [PersonalData]
        public ICollection<Issue> Issues { get; set; }
        [PersonalData]
        public ICollection <ProjectsUsers> ProjectsUsers { get; set; }
    }
}
