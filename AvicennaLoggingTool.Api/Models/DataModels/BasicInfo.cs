﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AvicennaLoggingTool.Api.Models.DataModels
{
    public abstract class BasicInfo
    {
        public long Id { get; set; }
        [Required]

        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public ApplicationUser Creater { get; set; }

    }
}
