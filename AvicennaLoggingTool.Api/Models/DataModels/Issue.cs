﻿using System;

namespace AvicennaLoggingTool.Api.Models.DataModels
{
    public class Issue:BasicInfo
    {
        public Status Status { get; set; }
        public Type Type { get; set; }
        public long UserId { get; set; }
        public Project Project { get; set; }
        public DateTime ?  Completed { get; set; }
      
    }
}
