﻿using System;

namespace AvicennaLoggingTool.Api.Models.DataModels
{
    public class LogTime
    {
        public long Id { get; set; }
        public DateTime Day { get; set; }
        public decimal Time { get; set; }
        public Issue Issue { get; set; }
    }
}
