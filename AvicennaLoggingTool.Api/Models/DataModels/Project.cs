﻿using System.Collections.Generic;

namespace AvicennaLoggingTool.Api.Models.DataModels
{
    public class Project:BasicInfo
    {
        public ICollection<Issue> Issues { get; set; }
        public ICollection<ProjectsUsers> ProjectsUsers { get; set; }
    }
}
