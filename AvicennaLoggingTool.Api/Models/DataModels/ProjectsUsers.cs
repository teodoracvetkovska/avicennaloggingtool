﻿namespace AvicennaLoggingTool.Api.Models.DataModels
{
    public class ProjectsUsers
    {
        public long ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public long ProjectId { get; set; }
        public Project Project { get; set; }

    }
}
