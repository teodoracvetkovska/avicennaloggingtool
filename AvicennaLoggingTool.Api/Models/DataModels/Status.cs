﻿using System.ComponentModel.DataAnnotations;

namespace AvicennaLoggingTool.Api.Models.DataModels
{
    public class Status
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
