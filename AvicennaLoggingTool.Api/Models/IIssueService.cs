﻿using AvicennaLoggingTool.Api.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.Models
{
    public interface IIssueService
    {
        IEnumerable<Issue> GetAllIssues();
        IEnumerable<Issue> GetAllIssuesByUser(long UserId);

        Issue GetIssueById(long id);

        void GiveIssueToUser(long IssueId, long userId);
        void ChageIssueStatus(long IssueId, string StatusName);

        void AddIssue(Issue task);
        void RemoveIssue(long taskId);
    }
}
