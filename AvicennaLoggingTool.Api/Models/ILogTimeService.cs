﻿using System;
using System.Collections.Generic;
using AvicennaLoggingTool.Api.Models.DataModels;

namespace AvicennaLoggingTool.Api.Models
{
    public interface ILogTimeService
    {
        LogTime GetLogTimeById(long id);
        IEnumerable<LogTime> GetAll();
        IEnumerable<LogTime> GetAllByUser(long id);
        void Add(LogTime logTime);
        void RemoveLogTime(long id);
        void EditLogTime(long id, LogTime newLogTime);
        IEnumerable<LogTime> GetAllBetween(DateTime from, DateTime to);
        IEnumerable<LogTime> GetAllBetweenByUserId(long id, DateTime from, DateTime to);
    }
}
