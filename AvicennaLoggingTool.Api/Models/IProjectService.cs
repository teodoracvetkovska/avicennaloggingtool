﻿using System.Collections.Generic;
using AvicennaLoggingTool.Api.Models.DataModels;

namespace AvicennaLoggingTool.Api.Models
{
    public interface IProjectService
    {
        IEnumerable<Project> GetAllProjects();
        IEnumerable<Project> GetAllProjectsByUser(long UserId);
        
        Project GetProjectsById(long ProjectId);

        void EditProject(Project project);

        void AddUsersToProject(long ProjectId, ICollection<long> UserIDs);
        void AddUserToProject(long ProjectId, long userId, bool? shouldSave);

        void RemoveUserFromProject(long ProjectId, long userId);
      
        void AddProject(Project project);
        void RemoveProject(long projectId);
    }
}
