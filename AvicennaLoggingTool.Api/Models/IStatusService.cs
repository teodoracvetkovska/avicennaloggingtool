﻿using System.Collections.Generic;
using AvicennaLoggingTool.Api.Models.DataModels;

namespace AvicennaLoggingTool.Api.Models
{
    public interface IStatusService
    {
        IEnumerable<Status> GetAll();
        Status Get(long id);
        void Add(Status newStatus);
    }
}
