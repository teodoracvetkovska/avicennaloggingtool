﻿
using AvicennaLoggingTool.Api.Models.DataModels;
using System.Collections.Generic;

namespace AvicennaLoggingTool.Api.Models
{
    public interface ITypeService
    {
        IEnumerable<Type> GetAll();
        Type Get(long id);
        void Add(Type newType);
    }
}
