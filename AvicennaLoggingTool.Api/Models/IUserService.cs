﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.Models
{
    public interface IUserService
    {
        ApplicationUser GetUserById(long userId);
    }
}
