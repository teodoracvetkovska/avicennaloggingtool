﻿using System;
using System.Collections.Generic;
using System.Linq;
using AvicennaLoggingTool.Api.Models;
using AvicennaLoggingTool.Api.Models.DataModels;
using Microsoft.EntityFrameworkCore;

namespace AvicennaLoggingTool.Api.Services
{
    public class IssueService : IIssueService
    {
        private ApplicationDbContext _context;
        private IUserService _userService;
        public IssueService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void AddIssue(Issue task)
        {
            _context.Add(task);
            _context.SaveChanges();
        }
        public IEnumerable<Issue> GetAllIssues()
        {
            return _context.Issues
                .Include(i => i.Status)
                .Include(i => i.Type)
                .Include(i => i.Project);
        }

        public IEnumerable<Issue> GetAllIssuesByUser(long UserId)
        {
            return GetAllIssues().Where(i => i.UserId == UserId);
        }
        public Issue GetIssueById(long id)
        {
            return GetAllIssues()
                .Where(i => i.Id == id)
                .FirstOrDefault();
        }

        public void GiveIssueToUser(long IssueId, long userId)
        {
            var User = _userService.GetUserById(userId);
            if (User == null) return;

            var Issue = GetIssueById(IssueId);
            if (Issue == null) return;
            _context.Update(User);
            _context.Update(Issue);

            User.Issues.Add(Issue);
            Issue.UserId = userId;

            _context.SaveChanges();
        }
        public void ChageIssueStatus(long IssueId, string StatusName)
        {
            var issue = GetIssueById(IssueId);

            _context.Update(issue);

            issue.Status.Name = StatusName;
            if (StatusName == "completed")
            {
                issue.Completed = DateTime.Now;
            }
            _context.SaveChanges();
        }
        public void RemoveIssue(long taskId)
        {
            var issue = GetIssueById(taskId);
            _context.Remove(issue);
            _context.SaveChanges();
        }


        
    }
}
