﻿using AvicennaLoggingTool.Api.Models;
using AvicennaLoggingTool.Api.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.Services
{
    public class LogTimeService : ILogTimeService
    {
        private ApplicationDbContext _context;

        public LogTimeService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Add(LogTime logTime)
        {
            _context.Add(logTime);
            _context.SaveChanges();
        }

        public void EditLogTime(long id, LogTime newLogTime)
        {
            var logTime = GetLogTimeById(id);
            _context.Update(logTime);
            if (logTime == null)
                return;
            logTime.Day = newLogTime.Day;
            logTime.Issue = newLogTime.Issue;
            logTime.Time = newLogTime.Time;
            _context.SaveChanges();
        }

        public IEnumerable<LogTime> GetAll()
        {
            return _context.LogTimes;
        }

        public IEnumerable<LogTime> GetAllBetween(DateTime from, DateTime to)
        {
            return GetAll().Where(x => x.Day >= from && x.Day <= to);
        }

        public IEnumerable<LogTime> GetAllBetweenByUserId(long id, DateTime from, DateTime to)
        {
            return GetAllByUser(id).Where(x => x.Day >= from && x.Day <= to);
        }

        public IEnumerable<LogTime> GetAllByUser(long id) 
        {
            return GetAll().Where(u => u.Id == id);
        }

        public LogTime GetLogTimeById(long id)
        {
            return GetAll().Where(x => x.Id == id).FirstOrDefault();
        }

        public void RemoveLogTime(long id)
        {
            var logTime = GetLogTimeById(id);
            if (logTime == null)
                return;
            _context.Remove(logTime);
        }
  
    }
}
