﻿using System;
using System.Collections.Generic;
using System.Linq;
using AvicennaLoggingTool.Api.Models;
using AvicennaLoggingTool.Api.Models.DataModels;
using Microsoft.EntityFrameworkCore;

namespace AvicennaLoggingTool.Api.Services
{
    public class ProjectService : IProjectService

    {
        private IUserService _userService;
        private ApplicationDbContext _context;
        public ProjectService(ApplicationDbContext context, IUserService userService)
        {
            _context=context;
            _userService = userService;
        }
        public void AddProject(Project project)
        {
            _context.Add(project);
            _context.SaveChanges();
        }
        public IEnumerable<Project> GetAllProjects()
        {
            return _context.Projects
                  .Include(p => p.Issues)
                  .Include(p=>p.ProjectsUsers);
               
        }
        public Project GetProjectsById(long ProjectId)
        {
            return GetAllProjects()
                .Where(p => p.Id == ProjectId)
                .FirstOrDefault();
        }
        public IEnumerable<Project> GetAllProjectsByUser(long UserId)
        {
            return GetAllProjects().Where(p => p.ProjectsUsers
            .Where(i => i.ApplicationUserId == UserId)!=null);
        }
        public void RemoveUserFromProject(long ProjectId, long userId)
        {
            var Project = GetProjectsById(ProjectId);
            var User = _userService.GetUserById(userId);

            _context.Update(Project);
            _context.Update(User);
          
           _context.ProjectsUsers
                .Remove(_context.ProjectsUsers
                .Where(p => p.ApplicationUserId == userId).First());

            _context.SaveChanges();
        }

        public void AddUsersToProject(long ProjectID,ICollection<long> userIds)
        {
            foreach(var id in userIds)
            {
                AddUserToProject(ProjectID, id); 
            }
            _context.SaveChanges();
        }

        public void AddUserToProject(long ProjectId, long userId,bool? shouldSave=false)
        {
            var Project = GetProjectsById(ProjectId);
            var User = _userService.GetUserById(userId);
            if (Project == null || User == null)
            {
                return;
            }
            _context.Update(Project);
            _context.Update(User);

            var projectUser = new ProjectsUsers()
            {
                Project=Project,
                ApplicationUser=User,
                ProjectId=ProjectId,
                ApplicationUserId=userId
            };

            _context.ProjectsUsers.Add(projectUser);

            if (shouldSave.Value)
            {
                _context.SaveChanges();
            }
           
        }
        public void RemoveProject(long projectId)
        {
            var project = GetProjectsById(projectId);
            _context.Remove(project);
            _context.SaveChanges();
        }
        public void EditProject(Project project)
        {
            _context.Update(project);
            _context.SaveChanges();
        }
    }
}
