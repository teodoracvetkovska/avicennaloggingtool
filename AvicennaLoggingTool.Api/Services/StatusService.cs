﻿using AvicennaLoggingTool.Api.Models;
using AvicennaLoggingTool.Api.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.Services
{
    public class StatusService : IStatusService
    {
        private ApplicationDbContext _context;

        public StatusService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(Status newStatus)
        {
            _context.Add(newStatus);
            _context.SaveChanges();
        }

        public Status Get(long id)
        {
            return GetAll().Where(d => d.Id == id).FirstOrDefault();
        }

        public IEnumerable<Status> GetAll()
        {
            return _context.Statuses.ToList();
        }
    }
}
