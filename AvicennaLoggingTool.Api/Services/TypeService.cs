﻿using AvicennaLoggingTool.Api.Models;
using AvicennaLoggingTool.Api.Models.DataModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.Services
{
    public class TypeService : ITypeService
    {
        private ApplicationDbContext _context;

        public TypeService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Add(Type newType)
        {
            _context.Add(newType);
            _context.SaveChanges();
        }

        public Type Get(long id)
        {
            return GetAll().Where(x => x.Id == id).FirstOrDefault();
        }

        public IEnumerable<Type> GetAll()
        {
            return _context.Types.ToList();
        }
    }
}
