﻿using AvicennaLoggingTool.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.Services
{
    public class UserService
    {
        private ApplicationDbContext _context;
        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }
        private ApplicationUser GetUserById(long userId)
        {
            return _context.Users.FirstOrDefault(i => i.Id == userId);
        }
    }
}
