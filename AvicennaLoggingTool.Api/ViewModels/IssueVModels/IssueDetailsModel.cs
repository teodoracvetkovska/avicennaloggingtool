﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.ViewModels.IssueVModels
{
    public class IssueDetailsModel
    {
        [Required]
        public string Status { get; set; }
        [Required]
        public string Type { get; set; }
        public string AssignedUser { get; set; }
        public string ProjectName { get; set; }
        public DateTime ? Completed { get; set; }
    }
}
