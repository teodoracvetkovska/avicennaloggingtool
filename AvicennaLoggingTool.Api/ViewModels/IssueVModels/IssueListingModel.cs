﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvicennaLoggingTool.Api.ViewModels.IssueVModels
{
    public class IssueListingModel
    {
        public IEnumerable<IssueDetailsModel> Issues { get; set; }
    }
}
