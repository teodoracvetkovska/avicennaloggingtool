﻿using System.ComponentModel.DataAnnotations;

namespace AvicennaLoggingTool.Api.ViewModels.ProjectVModels
{
    public class ProjectBindingModel
    {
        [Required]
        public long CreatorId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
