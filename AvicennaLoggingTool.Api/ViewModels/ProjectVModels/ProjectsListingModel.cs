﻿using AvicennaLoggingTool.Api.Models.DataModels;
using System.Collections.Generic;

namespace AvicennaLoggingTool.Api.ViewModels.ProjectVModels
{
    public class ProjectsListingModel
    {
        public IEnumerable<Project> Projects { get; set; }
    }
}
