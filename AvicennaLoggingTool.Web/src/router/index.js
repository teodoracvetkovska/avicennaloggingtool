import Vue from 'vue'
import Router from 'vue-router'
import Register from '@/components/public/Register'
import Login from '@/components/public/Login'
import Home from '@/components/app/Home'
import MyPage from '@/components/app/Mypage'
import Help from '@/components/app/Help'
import Projects from '@/components/app/Projects'
import ProjectAdd from '@/components/app/project/ProjectAdd'
import ProjectDetails from '@/components/app/project/ProjectDetail'
import ProjectEdit from '@/components/app/project/ProjectEdit'
import TaskAdd from '@/components/app/task/TaskAdd'
import TaskEdit from '@/components/app/task/TaskEdit'
import TaskDetails from '@/components/app/task/TaskDetails'
import LogTime from '@/components/app/task/LogTime'


Vue.use(Router)

export default new Router({
  routes: [{
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/app',
      name: 'Home',
      component: Home
    },
    {
      path: '/app/mypage',
      name: 'MyPage',
      component: MyPage
    },
    {
      path: '/app/help',
      name: 'Help',
      component: Help
    },
    {
      path: '/app/projects',
      component: Projects,
      name: 'Projects',
      children:[
        
      ]
    },
    {
      path: '/app/projects/add',
      name: 'ProjectAdd',
      component: ProjectAdd,
    },
    {
      path: '/app/projects/:id',
      name: 'ProjectDetails',
      component: ProjectDetails,
    },
    {
      path: '/app/projects/edit/:id',
      name: 'ProjectEdit',
      component: ProjectEdit,
    },
    {
      path:'/app/task/add',
      name:'TaskAdd',
      component:TaskAdd
    },
    {
      path:'/app/task/edit',
      name:'TaskEdit',
      component:TaskEdit
    },
    {
      path:'/app/task/details',
      name:'TaskDetails',
      component:TaskDetails
    },
    {
      path:'/app/log',
      name:'LogTime',
      component: LogTime
    },
  ]
})
