import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export const store = new Vuex.Store({
    state:{
        users:[
            {username: "Teodora", password: "password"},
            {username: "Jovan", password: "password"},      
            {username: "Dragan", password: "password"},
            {username: "Marija", password: "password"},        
        ]
    },
    getters:{
        loggedIn: state => param => state.users.find(el => el.username == param.username && el.password==param.password),
        
    },
       mutations:{
           addUser:(state,user)=>{
                console.log(state.users);
              state.users.push(user);
               
           }
       }
})